use std::collections::HashMap;

use tw_errors::{InternalError, TWError};

/// Component ID
pub type ComponentId = usize;

/// Component
#[derive(Debug)]
pub struct Component {}

impl Component {
    pub fn get_component(component: ComponentId) -> Result<Component, TWError> {
        Err(TWError::InternalError(InternalError::NotImplemented(String::from(
            "get_component",
        ))))
    }

    pub fn set_component(component: Component) -> Result<ComponentId, TWError> {
        Err(TWError::InternalError(InternalError::NotImplemented(String::from(
            "set_component",
        ))))
    }
}

/// Component system
#[derive(Debug)]
struct ComponentSystem {
    components: HashMap<ComponentId, Box<Component>>,
    ids: HashMap<ComponentTypeId, ComponentId>,
}

impl ComponentSystem {
    fn new() -> ComponentSystem {
        ComponentSystem {
            components: HashMap::new(),
            ids: HashMap::new(),
        }
    }
}

/// Component Type ID
#[derive(Debug)]
pub struct ComponentTypeId {
    type_id: usize,
}

impl ComponentTypeId {
    pub fn new<ComponentType>(component: ComponentType) -> ComponentTypeId
    where
        ComponentType: GetComponentTypeId,
    {
        ComponentTypeId {
            type_id: component.get_component_type_id(),
        }
    }
}

/// GetComponentTypeId trait
///
/// Implemented by all components to have a uniform way of retrieving the type id for a
/// component.
pub trait GetComponentTypeId {
    fn get_component_type_id(&self) -> usize;
}
