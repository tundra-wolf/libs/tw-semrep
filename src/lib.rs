//! This library provides the semantic representation of the project's build and
//! deliverables. A semantic representation is stored in the `Project` structure, which
//! contains the following data.
//!
//! * Build
//!
//!     The build process is fully analysed and stored in this object. This is used to
//!     analyse the effect of configuration. It is also used to be able to fully analyse
//!     sources and documentation generated during the build process.
//!
//! * Programs
//!
//!     All programs/scripts/servers/daemons that are part of the project's deliverables.
//!
//! * Libraries
//!
//!     All libraries that are part of, or build from source code as part of the
//!     project's deliverables.
//!
//! * Documents
//!
//!     All documents that are part of the project's deliverables.
//!
//! The main objects are the following.
//!
//! * mod `deliverable::documentation`
//!     * Document
//!         * Part
//!             * Chapter
//!                 * Section
//!                     * Paragraph
//!         * Header
//!
//! * mod `deliverable::documentation::text`
//!     * Text
//!
//! * mod `deliverable::documentation::format`
//!     * Format
//!         * FormatRule
//!
//! * mod `deliverable::library`
//!     * Library
//!
//! * mod `deliverable::program`
//!     * Program
//!
//! * mod `ecs`
//!     * ComponentSystem
//!     * Component
//!
//! * mod `project`
//!     * Project
//!
//! * mod `project::build`
//!     * Build
//!
//! * mod `project::configuration`
//!     * Configuration
//!         * Setting
//!
//! * mod `project::source`
//!     * Source
//!         * SourceKind
//!         * SourceText
//!
//! * mod `source_code::semantic_graph`
//!     * SemanticGraph
//!
//! * mod `source_code::symbol_table`
//!     * SymbolTable
//!
//! * mod `source_code::syntax_tree`
//!     * SyntaxTree
//!
//! The semantic representation, because a lot of objects are reused, uses a component
//! system to store its data. In the different structures the following components get
//! IDs when referenced elsewhere.
//!
//! * Document / DocumentId (deliverable::documentation::Document)
//! * Format / FormatId (deliverable::documentation::format::Format)
//! * Library / LibraryId (deliverable::library::Library)
//! * Program / ProgramId (deliverable::program::Program)

pub mod deliverable;
pub mod ecs;
pub mod project;
pub mod source_code;
