pub mod build;
pub mod source;

use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::deliverable::documentation::Document;

/// Project
#[derive(Debug, Deserialize, Serialize)]
pub struct Project {
    name: String,
    documents: HashMap<String, Document>,
}
