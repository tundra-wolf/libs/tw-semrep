use serde::{Deserialize, Serialize};

use crate::deliverable::documentation::format::FormatId;

/// Stores a piece of text with the same formatting
#[derive(Debug, Deserialize, Serialize)]
pub struct Text {
    text: String,
    format: FormatId,
}
