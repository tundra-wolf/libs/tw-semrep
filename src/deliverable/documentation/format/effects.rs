use serde::{Deserialize, Serialize};

/// Lines
#[derive(Debug, Deserialize, Serialize)]
pub struct Line {
    pattern: Pattern,
    size: f32,
    position: Position,
}

/// Pattern
#[derive(Debug, Deserialize, Serialize)]
pub struct Pattern {}

/// Position from the position indicated. Position are stored in percentages of the font
/// size, relative to the position. Some examples.
///
/// * 10% from the top: Top(10)
/// * 10% from the bottom: Bottom(10)
/// * 10% below the center: Center(-10)
///
/// This means the following settings are the same: Top(0), Bottom(100), Center(50)
#[derive(Debug, Deserialize, Serialize)]
pub enum Position {
    Bottom(f32),
    Center(f32),
    Top(f32),
}
