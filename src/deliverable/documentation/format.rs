pub mod alignment;
pub mod effects;
pub mod font;

use serde::{Deserialize, Serialize};

use crate::ecs::ComponentId;

/// Format ID
pub type FormatId = ComponentId;

/// Stores a text format
#[derive(Debug, Deserialize, Serialize)]
pub struct Format {
    id: FormatId,
    rules: Vec<FormatRule>,
}

/// Stores a formatting rule
#[derive(Debug, Deserialize, Serialize)]
pub enum FormatRule {}
