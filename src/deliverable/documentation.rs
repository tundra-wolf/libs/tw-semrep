pub mod format;
pub mod text;

use serde::{Deserialize, Serialize};

use crate::deliverable::documentation::text::Text;
use crate::ecs::ComponentId;
use crate::project::source::SourceKind;

/// Document ID
pub type DocumentId = ComponentId;

/// Document structure
#[derive(Debug, Deserialize, Serialize)]
pub struct Document {
    id: DocumentId,
    title: Text,
    subtitles: Vec<Text>,
    kind: SourceKind,
}
