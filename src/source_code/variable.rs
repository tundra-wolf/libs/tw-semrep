use crate::source_code::literal::Value;
use crate::source_code::symbol_table::SymbolId;

/// Stores a variable
pub struct Variable {
    name: String,
    kind: SymbolId,
    mutability: Mutability,
    reference_kind: ReferenceKind,
    initial_value: Value,
}

/// Variable modifiers
pub enum Mutability {
    Constant,
    Immutable,
    Mutable,
    Static,
}

/// Reference kind
pub enum ReferenceKind {
    None,
    Pointer,
    RawPointer,
    Reference,
}
