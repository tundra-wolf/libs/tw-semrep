use std::collections::HashMap;

use crate::source_code::literal::Literal;
use crate::source_code::variable::Variable;

/// Symbol identifier
pub type SymbolId = usize;

/// Symbol table
pub struct SymbolTable {
    data: HashMap<SymbolId, Symbol>,
}

/// Symbol
pub struct Symbol {
    scope: SymbolId,
    data: SymbolData,
}

/// Symbol data
pub enum SymbolData {
    Variable(Variable),
    Literal(Literal),
}
