//! Storing literal values
//!
//! Literal values are values explicitly stated in the source code. The ability to give
//! literals in programming language differs. Most support standard characters, strings
//! and numbers. Some will also support arrays or dictionaries. Others will support
//! literals for any type.
//!
//! Literals are stored in the `Literal` object. Values of scalar types are stored in
//! the `Value` object.

use tw_data::types::{
    AsciiString, Char16, Char32, Decimal32, Decimal64, String16, String32,
};

use crate::source_code::symbol_table::SymbolId;

/// Stores a value
///
/// Any scalar type is represented here. Most string types (even though they are often
/// arrays) are also represented here.
pub enum Value {
    // Boolean
    Boolean(bool),

    // Character types
    AsciiChar(u8),
    Char8(char),
    Char16(Char16),
    Char32(Char32),

    // Decimal types
    Decimal32(Decimal32),
    Decimal64(Decimal64),

    // Floating point types
    Float32(f32),
    Float64(f64),

    // Integers (Signed)
    Int8(i8),
    Int16(i16),
    Int32(i32),
    Int64(i64),
    Int128(i128),

    // Integers (Unsigned)
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    UInt64(u64),
    UInt128(u128),

    // Strings
    AsciiString(AsciiString),
    String8(String),
    String16(String16),
    String32(String32),
}

/// Stores a literal
pub enum Literal {
    Value(SymbolId, Value),
    Array(ArrayLiteral),
    Structure(StructureLiteral),
    //Dictionary(Vec<Type>, Record<Literal>),
}

/// Stores an array
pub struct ArrayLiteral {
    kind: SymbolId,
    data: Vec<Literal>,
}

/// Stores a structure
pub struct StructureLiteral {
    kind: SymbolId,
    data: Vec<(SymbolId, Literal)>,
}
