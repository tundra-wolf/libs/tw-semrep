/// Data type
pub enum Kind {
    Boolean,
    Int8,
    Int16,
    Int32,
    Int64,
}
