pub mod database;
pub mod document;
pub mod language;

use std::path::PathBuf;

use serde::{Deserialize, Serialize};

use crate::ecs::GetComponentTypeId;
use crate::project::source::database::DatabaseKind;
use crate::project::source::document::DocumentKind;
use crate::project::source::language::{LanguageKind, ScriptKind};

/// Source
#[derive(Debug, Deserialize, Serialize)]
pub struct Source {
    data: SourceText,
    kind: SourceKind,
}

/// Recognized source types
#[derive(Debug, Deserialize, Serialize)]
pub enum SourceKind {
    Database(DatabaseKind),
    Document(DocumentKind),
    Language(LanguageKind),
    Script(ScriptKind),
}

impl GetComponentTypeId for SourceKind {
    fn get_component_type_id(&self) -> usize {
        match self {
            SourceKind::Database(database) => 10000 + database.get_component_type_id(),
            SourceKind::Document(document) => 20000 + document.get_component_type_id(),
            SourceKind::Language(language) => 30000 + language.get_component_type_id(),
            SourceKind::Script(script) => 40000 + script.get_component_type_id(),
        }
    }
}

/// Source text. The location of the source text. The following are supported.
///
/// * Database - Query to retrieve the source text
/// * File - Path to the file storing the source text
/// * Text - Source text
#[derive(Debug, Deserialize, Serialize)]
pub enum SourceText {
    Binary(Vec<u8>),
    Database(String),
    File(PathBuf),
    Text(String),
}
