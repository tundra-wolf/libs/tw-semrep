use serde::{Deserialize, Serialize};

use crate::ecs::GetComponentTypeId;

/// Recognized programming languages
#[derive(Debug, Deserialize, Serialize)]
pub enum LanguageKind {
    C,
    CSharp,
    PowerScript,
    Rust,
    VisualBasic,
}

impl GetComponentTypeId for LanguageKind {
    fn get_component_type_id(&self) -> usize {
        match self {
            LanguageKind::C => 1,
            LanguageKind::CSharp => 2,
            LanguageKind::PowerScript => 3,
            LanguageKind::Rust => 4,
            LanguageKind::VisualBasic => 5,
        }
    }
}

/// Recognized script types
#[derive(Debug, Deserialize, Serialize)]
pub enum ScriptKind {
    BashShell,
    Javascript,
    KornShell,
    Perl,
    Powershell,
    Python,
}

impl GetComponentTypeId for ScriptKind {
    fn get_component_type_id(&self) -> usize {
        match self {
            ScriptKind::BashShell => 1,
            ScriptKind::Javascript => 2,
            ScriptKind::KornShell => 3,
            ScriptKind::Perl => 4,
            ScriptKind::Powershell => 5,
            ScriptKind::Python => 6,
        }
    }
}
