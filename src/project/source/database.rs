use serde::{Deserialize, Serialize};

use crate::ecs::GetComponentTypeId;

/// Database language. The supported languages are the following.
///
/// * Document - Query language for document databases
/// * Graph - Query language for graph databases
/// * SQL - Query language for relational databases
#[derive(Debug, Deserialize, Serialize)]
pub enum DatabaseKind {
    Document,
    Graph,
    Sql,
}

impl GetComponentTypeId for DatabaseKind {
    fn get_component_type_id(&self) -> usize {
        match self {
            DatabaseKind::Document => 1,
            DatabaseKind::Graph => 2,
            DatabaseKind::Sql => 3,
        }
    }
}
