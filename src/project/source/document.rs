use serde::{Deserialize, Serialize};

use crate::ecs::GetComponentTypeId;

/// Recognized document types
#[derive(Debug, Deserialize, Serialize)]
pub enum DocumentKind {
    AsciiDoc,
    Css,
    DocBook,
    Html,
    Markdown,
    OpenDocumentFormat,
    OfficeOpenXml,
    Tex,
    Troff,
}

impl GetComponentTypeId for DocumentKind {
    fn get_component_type_id(&self) -> usize {
        match self {
            DocumentKind::AsciiDoc => 1,
            DocumentKind::Css => 2,
            DocumentKind::DocBook => 3,
            DocumentKind::Html => 4,
            DocumentKind::Markdown => 5,
            DocumentKind::OpenDocumentFormat => 6,
            DocumentKind::OfficeOpenXml => 7,
            DocumentKind::Tex => 8,
            DocumentKind::Troff => 9,
        }
    }
}
