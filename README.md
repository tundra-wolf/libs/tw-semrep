# Tundra Wolf Semantic Representation library

This library provides the generic semantic representation of the project being processed. It stores the syntactic and semantic structure, to be processed when refactoring and converting the project.

## Overview

The library contains a type for each object in the source project. Each of these objects stores the details of that object. The entire project is stored in the type `SemanticRepresentation`.

The following lists all of the main types defined in this library.

* `SemanticRepresentation` - Representation of the whole project

* `Project` - Project
    * `Build` - Build process

* `Document`

* `Object` - Runtime object
* `Code`
    * `Loop`
    * `Selection`
    * `Sequence`
* `Macro` - Compile time executed content
* `Type`
    * `Structure`
    * `Enumeration`
    * `Function`
    * `Method`

## Mapping Source to Semantic Representation

When parsing/verifying the source project, this is done in the source format. To store it in the semantic representation the `From` trait is used. This means each parsed object's source representation has this trait implemented to automate the conversion.

Types and functions used in these traits are in the module `from::source` of this library.
